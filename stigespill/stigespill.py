from components import Dice, Board, Players, Ladder

SQUARES = 100
SIDES = 6
COLORS = {
    0: 'red',
    1: 'green',
    2: 'yellow',
    3: 'blue',
    4: 'pink',
    5: 'orange'
}
LADDERS = {
    1: 21,
    3: 25,
    16: 42,
    20: 60,
    26: 52,
    34: 62,
    69: 90,
    77: 99
}
SNAKES = {
    97: 21,
    66: 50,
    63: 44,
    37: 18,
    17: 0
}


class Game:
    def __init__(self, players, sqr_length, dice_sides, ladders, snakes):
        self.dice = Dice(dice_sides)
        self.players = [Players(COLORS[i]) for i in range(players)]
        self.board = Board(sqr_length)
        self.ladders = {start: Ladder(start, ladders[start]) for start in ladders}
        self.snakes = {start: Ladder(start, snakes[start]) for start in snakes}
        self.last_pos = sqr_length-1
        self.finished = False

    def make_move(self, player):
        dice_roll = self.dice.roll()
        player.move(dice_roll)
        return player.get_color(), dice_roll

    def check_finished(self, player):
        if player.pos >= self.last_pos:
            return True
        else:
            return False

    def do_round(self):
        for player in self.players:
            move = input(f'Press enter to roll for {player.get_color()}, anything else to stop: ')
            if move == '':
                player_color, number_of_sqr = self.make_move(player)
                print(f'{player_color} moved {number_of_sqr} positions')
                if player.get_pos() in self.ladders:
                    print(f"You landed on a ladder! You're moving from {player.get_pos()} to {LADDERS[player.get_pos()]}")
                    self.ladders[player.get_pos()].move_player(player)
                if player.get_pos() in self.snakes:
                    print(f"You landed on a snake! You're moving from {player.get_pos()} to {SNAKES[player.get_pos()]}")
                    self.snakes[player.get_pos()].move_player(player)
                print('New position:')
                print(f'{player.get_pos()}')
                if self.check_finished(player):
                    print(f'{player_color} won')
                    self.finished = True
                    break
            else:
                self.finished = True
                break

    def check_all_player(self):
        not_end = True
        for p in self.players:
            if self.check_finished(p):
                not_end = False
        return not_end

#Game object takes 5 arguments, nr of players, nr of squares, sides on the dice, ladders and snakes
def main():
    spill = Game(2, SQUARES, SIDES, LADDERS, SNAKES)
    while not spill.finished:
        spill.do_round()


if __name__ == "__main__":
    main()
